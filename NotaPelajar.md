# SSIS

## Alert for Cloning
git clone --branch master https://gitlab.com/isk6708/ssis.git

## Rujukan
- Online - https://learn.microsoft.com/en-us/sql/integration-services/ssis-how-to-create-an-etl-package?view=sql-server-ver16
- eBook - 
https://drive.google.com/file/d/1Hr9C7h7ZfIQp2P6tZvS6jN9lC2L8MXic/view?usp=sharing

## Pemasangan / Keperluan
- SQL Server - MySQL
- SSMS - SQL Server Management Studio - Untuk tujuan belajar better ada
- SSDT - https://learn.microsoft.com/en-us/sql/ssdt/download-sql-server-data-tools-ssdt?view=sql-server-ver16
- Visual Studio 2022 - 

## Training material - extra

GutHub - https://github.com/MicrosoftDocs/sql-docs/blob/live/docs/integration-services/lesson-1-create-a-project-and-basic-package-with-ssis.md

- Lesson 1 - https://learn.microsoft.com/en-us/sql/integration-services/lesson-1-create-a-project-and-basic-package-with-ssis?view=sql-server-ver16


## Extra Configurations

```
<DTS:Configurations>
    <DTS:Configuration
      DTS:ConfigurationString="C:\RajWork\2023\Alami\SSIS\ICU\SSIS Tutorial\SSISTutorial.dtsConfig"
      DTS:ConfigurationType="1"
      DTS:CreationName=""
      DTS:DTSID="{832F38FF-6D7A-4E8E-AD74-699886242BCE}"
      DTS:ObjectName="SSIS Tutorial Directory configuration" />
  </DTS:Configurations>
  ```

  - Bagi SSMS yang berbeza version dengan SQL Server
  - https://www.mssqltips.com/sqlservertip/6479/sql-server-management-studio-connection-to-integration-services-error-class-not-registered/#:~:text=The%20issue%20can%20be%20fixed,connect%20to%20the%20SSIS%20service.
  
